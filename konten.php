<!DOCTYPE html>
<html>
<head>
<title>Kesehatan Pria Dewasa</title>
</head>
<body>

<h1>Varikokel, Kondisi pada Pria yang Bisa Hambat Program Hamil</h1>

<img src="image\varikokel.jpg" alt="pria dewasa" width="250" height="250">
<p>Jakarta - Saat sedang program hamil ada beberapa kondisi pada pria yang bisa jadi hambatan. Salah satunya varikokel, Apa itu?<p>
Dijelaskan dr Nur Hafiz Ramadhona SpAnd, sebelum menjalani program hamil pria perlu melakukan tes andrologi. Pada dasarnya andrologi adalah cabang ilmu kedokteran yang membahas mengenai laki-laki mulai dari infertilitas, seksual, KB pria bahkan penuaan. Saat pasangan suami istri akan menjalani program hamil istri biasanya akan ke dokter kandungan dan suami ke dokter andrologi.<p>
Nah, parameter di sini adalah sperma, karena selama sperma bagus berarti sudah bagus atau oke untuk program hamil sisanya tergantung dari pihak istri," kata pria yang akrab disapa dr Rama ini di tengah sesi media visit di RS YPK Mandiri, Menteng, Jakarta Pusat baru-baru ini.<p>


dr Rama mengatakan, untuk melihat kualitas sperma bagus atau tidak perlu dilihat apakah sperma bermasalah atau nggak. Jika ada, maka masalah tersebut harus disingkirkan.<p>

"Misal, apakah ada faktor varikokel, apa ada sumbatan atau lainnya di kemaluan. Varikokel bisa disebut varises, bagi awam emang tahunya varises itu hanya di kaki atau beberapa bagian tubuh nah kalau varikokel ini adanya di kantong kemaluan. Jadi testis kalau mau menghasilkan sperma yang bagus butuh suhu yang lebih dingin daripada suhu tubuh sedangkan varikokel itu memberi suhu yang panas pada testis," papar dr Rama.<p>


Penanganan varikokel bisa dengan pemberian obat sampai operasi. Jika memang operasi, setelahnya akan dilakukan pengecekan untuk melihat apakah kualitas sperma membaik atau tidak. dr Rama mengatakan penyebab varikokel umumnya karena pria sering melakukan aktivitas berat seperti angkat beban.<p>

"Karena varikokel sendiri itu kan seperti munculnya varises di kaki kan jadi seperti masalah pembuluh darah. Kedua, bisa disebabkan dari pola hidup juga, jarang olahraga dan makan nggak sehat sehingga kolesterol menumpuk," tutur dr Rama.<p>

Ilustrasi varikokelIlustrasi varikokel/ Foto: thinkstock
Sama halnya seperti masalah jantung, darah yang bersifat panas, jika susah naik maka akan berkumpul pada bagian tersebut. Begitu juga pada bagian testis, jika darah tidak mengalir lancar, membentuk varikokel sehingga menaikkan suhu testis, dan karena itu bisa membuat kualitas sperma nggak bagus, sehingga bisa mempersulit proses program hamil.<p>

Dikutip dari detikcom dr Nur Rasyid, SpU(K)mengatakan umumnya hanya dokter yang bisa mengidentifikasi varikokel dengan tepat. Kantung kemaluan terlalu tebal dan warnanya terlalu gelap untuk diamati sendiri.<p>

Para grade 1, varikokel bisa diraba hanya jika pasien mengejan. Grade 2, varikokel kelihatan dengan kasat mata saat pasien mengejan. Nah pada grade 3, barulah varikokel terlihat jelas. Jika diameter vena lebih dari 2 mm, operasi mutlak harus dilakukan.<p>

"Gejala varikokel paling mudah dilihat setelah menikah. Kalau ada masalah kesuburan, kemungkinan ada varikokel," ujar dr Nur Rasyid.<p>
Amelia Sewaka Kamis, 27 Sep 2018 16:05 WIB</p>

</body>
</html>